#!/usr/bin/env bash

# Delete references to SVN branches that no longer exist
# Usage (this will not execute without asking you to confirm the list):
# bin/git-svn-prune.sh

# Started with 

# Set your SVN prefix
GIT_SVN_PREFIX="$1"

SVN_REPO=$(git config --get svn-remote.svn.url)
GIT_SVN_PREFIX_SED=$(echo "$GIT_SVN_PREFIX" | sed -e 's/[\/&]/\\&/g')
SVN_BRANCHES_PATH=$(git config --get svn-remote.svn.branches | sed 's|/[*].*||')
SVN_BRANCHES_URL="$SVN_REPO/$SVN_BRANCHES_PATH"
TEMP_DIR=$(mktemp -d -t git-svn-prune)
SVN_BRANCHES_FILE="$TEMP_DIR/svn-branches"
GIT_BRANCHES_FILE="$TEMP_DIR/git-branches"
OLD_BRANCHES_FILE="$TEMP_DIR/old-branches"

echo $TEMP_DIR
echo $SVN_BRANCHES_URL

svn ls "$SVN_BRANCHES_URL" | sed 's|^[[:space:]]*||' | sed 's|/$||' > "$SVN_BRANCHES_FILE"

if [[ ! -s "$SVN_BRANCHES_FILE" ]]
then
    echo "No remote SVN branches found at \"$SVN_BRANCHES_URL\". Check configuration."
    exit 1
fi

git branch -r | sed 's|^[[:space:]]*||' > "$GIT_BRANCHES_FILE"
if [[ $GIT_SVN_PREFIX ]]
then
    sed -i -e 's/^'"$GIT_SVN_PREFIX_SED"'//' "$GIT_BRANCHES_FILE"
fi
sed -i -e '/^tags/d;/\//d;/trunk/d' "$GIT_BRANCHES_FILE"
# echo "bad-branch" >> "$GIT_BRANCHES_FILE"
# echo "another-bad-branch" >> "$GIT_BRANCHES_FILE"

if [[ ! -s "$GIT_BRANCHES_FILE" ]]
then
    echo "Your local git repository contains no references to any SVN branches at all, deleted or not. Check configuration."
    exit 1
fi

diff -u "$GIT_BRANCHES_FILE" "$SVN_BRANCHES_FILE" | grep '^-' | sed 's|^-||' | grep -v '^--' > "$OLD_BRANCHES_FILE"
if [[ $GIT_SVN_PREFIX ]]
then
    sed -i -e 's/^/'"$GIT_SVN_PREFIX_SED"'/' "$OLD_BRANCHES_FILE"
fi

if [[ -s "$OLD_BRANCHES_FILE" ]]
then

    echo "References to deleted SVN branches were found in your local git repository:"
    echo
    cat "$OLD_BRANCHES_FILE" | sed 's/^/   - /'
    echo
    read -p 'Delete the references from local git respository? Press "y" or "n" ' -n 1 -r
    echo

    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        echo
        for BRANCH in `cat "$OLD_BRANCHES_FILE"`
        do
            echo "Deleting $BRANCH ..."

            # UNCOMMENT THE FOLLOWING TWO LINES TO ENABLE SCRIPT
           git branch -d -r "$BRANCH"
           rm -rf .git/svn/refs/remotes/"$BRANCH"
        done

        # GREEN='\033[0;32m'
        # NC='\033[0m'
        # echo
        # echo -e "${GREEN}Note: This was only a test. This script is not stable enough yet to actually do anything on its own.${NC}"
        # echo -e "${GREEN}      If the list of deleted branches was accurate, then open the script up in an editor,${NC}"
        # echo -e "${GREEN}      uncomment the lines that actually delete the branches, then execute the script again.${NC}"
    fi

else
    echo "Your local git repository contains no references to deleted SVN branches."
fi ;
